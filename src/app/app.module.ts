import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';

import { PostsFormComponent } from './posts/posts-form/posts-form.component';
import { SinglePostComponent } from './posts/single-post/single-post.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { HttpClientModule} from "@angular/common/http";
import {PostsService} from "./services/posts.service";
import {RouterModule, Routes} from "@angular/router";
import {PostsListComponent} from "./posts/posts-list/posts-list.component";

const appRoutes : Routes = [
  {path: 'liste', component: PostsListComponent },
  {path: 'creation-de-post', component: PostsFormComponent },
  { path: '', redirectTo: 'liste', pathMatch: 'full' },
  { path: '**', redirectTo: 'liste' }
  ];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    PostsListComponent,
    PostsFormComponent,
    SinglePostComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [PostsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
