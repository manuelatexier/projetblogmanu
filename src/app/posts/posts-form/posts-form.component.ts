import { Component, OnInit } from '@angular/core';
import {PostsService} from "../../services/posts.service";
import {Router} from "@angular/router";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-posts-form',
  templateUrl: './posts-form.component.html',
  styleUrls: ['./posts-form.component.css']
})
export class PostsFormComponent implements OnInit {

  postForm: FormGroup;

  constructor(private router: Router, private postService: PostsService, private formBuilder : FormBuilder) { }

  ngOnInit() {
    this.initForm();
  }

  initForm(){
    this.postForm = this.formBuilder.group({
      title:['',Validators.required],
      content:['', Validators.required]
    })
  }

  onSubmit(){
    const title = this.postForm.get('title').value;
    const content = this.postForm.get('content').value;
    this.postService.createNewPost(title, content);
    this.router.navigate(['liste']);
  }

}
