import { Component } from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'blogProjectUpdate';
  constructor(){

    // Initialize Firebase
    var config = {
      apiKey: "AIzaSyCDfJ_I50n7E2FHDhufSO8Nz04hSqWUeYU",
      authDomain: "projet-blog-44d5b.firebaseapp.com",
      databaseURL: "https://projet-blog-44d5b.firebaseio.com",
      projectId: "projet-blog-44d5b",
      storageBucket: "projet-blog-44d5b.appspot.com",
      messagingSenderId: "965776103065"
    };

    firebase.initializeApp(config);

  }
}
