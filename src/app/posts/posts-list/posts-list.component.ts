import {Component, OnDestroy, OnInit} from '@angular/core';
import {Post} from "../../models/post.model";
import {Subscription} from "rxjs/index";
import {PostsService} from "../../services/posts.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-posts-list',
  templateUrl: './posts-list.component.html',
  styleUrls: ['./posts-list.component.css']
})
export class PostsListComponent implements OnInit, OnDestroy {

  posts: Post[];
  postsSubscription: Subscription;

  constructor(private postsService: PostsService, private router: Router) { }

  ngOnInit() {

    this.postsSubscription = this.postsService.postsSubject.subscribe(
      (posts: Post[]) => {
        this.posts = posts;
      }
    )

    this.postsService.emitPosts();

    }
// nouveau post redirection vers la page de formulaire

  onNewPost(){
    this.router.navigate(['creation-de-post'])
  }

  onRemovePost(index: number){
    index=index;
    this.postsService.deletePost(index);
  }

    //add love or remove love
addLove(index : number){
    index = index;
    this.postsService.onAddLove(index);
}

removeLove(index : number){
    index = index;
    this.postsService.onRemoveLove(index);
  }




  ngOnDestroy(){
    this.postsSubscription.unsubscribe();
  }




}
