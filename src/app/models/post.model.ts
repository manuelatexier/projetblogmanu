/**
 * Created by clari on 21/08/2018.
 */
export class Post {

  loveIts: number;
 created_at: number;

  constructor (public title: string, public content: string){

    this.created_at = Date.now();
    this.loveIts = 0;
  }

}
