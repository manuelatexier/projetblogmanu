import { Injectable } from '@angular/core';
import {Post} from "../models/post.model";
import {Subject} from "rxjs/index";
import * as firebase from "firebase";
import DataSnapshot = firebase.database.DataSnapshot;


@Injectable({
  providedIn: 'root'
})
export class PostsService {




  posts: Post[] = [];
  postsSubject = new Subject<Post[]>();

  constructor() {
    this.getPosts();
  }

  emitPosts(){
    this.postsSubject.next(this.posts)
  }

  //Dialogue serveur

  savePosts(){
    firebase.database().ref('/posts').set(this.posts);
  }

  getPosts() {
    firebase.database().ref('/posts')
      .on('value', (data: DataSnapshot) => {
          this.posts = data.val() ? data.val() : [];
          this.emitPosts();
        }
      );
  }


  //Posts Creation and destruction

  createNewPost(title: string, content: string){
    const newPost = new Post(title, content);
    this.posts.push(newPost);
    this.savePosts();
    this.emitPosts();
  };

  deletePost(index: number){
    this.posts.splice(index,1);
    this.savePosts();
    this.emitPosts();
  };

  //Amour ou désamour

  onAddLove(index: number){
    const post: Post = this.posts[index];
    post.loveIts++;
    this.savePosts();
    this.emitPosts();
  }

  onRemoveLove(index: number){
    const post: Post = this.posts[index];
    post.loveIts--;
    this.savePosts();
    this.emitPosts();
  }




}
